**COMP 7881**
*Advanced Topis in Software Engineering (DevOps)*

This repo was created to test the Bitbucket Tool as a Version Control tool.
---

## Steps

1. Create Atlasisian Account
2. Install Bitbucket and Jira
   Bitbucket for version control and Jira for Issues Tracking
3. Create Repository
   Project Name = COMP_7881
   Repo Name = assignment_1
4. Added issues to Jira
5. Linked VSCode to both Bitbucket and Jira to test integrations
6. Create a branch
7. Created new file (sample_file.txt)
8. Update README.md file to include purpose and steps
9. Commit the changes into the branch
10. Merge the branch into Main Branch 

---